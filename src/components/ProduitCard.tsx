import { IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonGrid, IonImg, IonRow } from '@ionic/react';
import React, { useContext, useEffect, useState } from 'react';
import AppContext from '../data/app-context';

import { ROUTE_FICHE } from '../nav/Routes';
import defaultImg from '../../public/assets/default.png';
import './ProduitCard.scss'
import { Plugins, FilesystemDirectory, Filesystem } from '@capacitor/core';

const ContentCard: React.FC<{ produitId: string }> = (props) => {
    const appCtx = useContext(AppContext);
    const [pictureBase64, setPictureBase64] = useState<string>();
  
    const produit = appCtx.produit.find(produit => produit.id === props.produitId)
    
  
    const updateBase64 = async () => {
      if (produit?.pictures !== undefined && produit?.pictures?.length > 0) {
        const file = await Filesystem.readFile({
          path: produit.pictures[0],
          directory: FilesystemDirectory.Data
        })
        setPictureBase64('data:image/jpeg;base64,' + file.data)
      } else {
        setPictureBase64(undefined)
      }
    }
  
    useEffect(() => {
      updateBase64()
    }, [produit?.pictures])


    return (
        <React.Fragment>
          {
            produit &&
            <IonCard routerLink={ROUTE_FICHE + produit?.id}>
              <img src={pictureBase64 ? pictureBase64 : defaultImg} />
              <IonCardHeader>
                <IonCardSubtitle>
                  <IonGrid className="ion-no-padding">
                    <IonRow className="ion-justify-content-between">
                      <IonCol size="auto">{produit?.name}€</IonCol>
                      
                    </IonRow>
                  </IonGrid>
                </IonCardSubtitle>
                <IonCardTitle mode="md">{produit?.stock}</IonCardTitle>                
              </IonCardHeader>
            </IonCard>
          }
        </React.Fragment>
    
    
      );
    };
export default ContentCard;
