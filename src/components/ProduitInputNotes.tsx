import { IonAlert, IonItem, IonLabel, IonNote } from '@ionic/react';
import React, { useContext, useState } from 'react';
import AppContext, { Produit } from '../data/app-context';


const ApartmentInputNotes: React.FC<{ produit: Produit }> = (props) => {
    const appCtx = useContext(AppContext)
    const [showAlert, setShowAlert] = useState(false);

    const update = (data: number) => {
        let updateProduit = { ...props.produit }
        updateProduit.stock = data;
        appCtx.updateProduit(updateProduit);
    }
    return (
        <IonItem className="ion-margin-bottom">
            <IonLabel onClick={() => setShowAlert(true)}>
                <p>{props.produit.stock}</p>
            </IonLabel>
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                header='stock'
                inputs={[
                    {
                        name: 'stock',
                        type: 'textarea',
                        id: `produit-detail`,
                        value: props.produit.stock,
                        placeholder: 'Your detail'
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (alertData) => update(alertData['notes'])
                    }
                ]} />
        </IonItem>
    )
}

export default ApartmentInputNotes