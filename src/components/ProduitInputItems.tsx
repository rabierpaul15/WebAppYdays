import { IonAlert, IonItem, IonLabel, IonNote } from '@ionic/react';
import React, { useContext, useState } from 'react';
import AppContext, { Produit, ProduitInputFields } from '../data/app-context';


const ContentInputItems: React.FC<{ field:ProduitInputFields, produit: Produit, friendlyName: string, unit: string }> = (props) => {
    const appCtx = useContext(AppContext)
    const [showAlert, setShowAlert] = useState(false);

    const update = (data: number | string) => {
        let updatedProduit = {...props.produit }
        if (props.field === "price" || props.field === "stock" ){
            updatedProduit[props.field] = +data;
        }else{
            updatedProduit[props.field] = String(data);
        }
        
        appCtx.updateProduit(updatedProduit);
    }
    return (
        <IonItem>
            <IonLabel>
                {props.friendlyName}
            </IonLabel>
            <IonNote onClick={() => setShowAlert(true)} slot="end">{props.produit[props.field]}{props.unit}</IonNote>
            <IonAlert
                isOpen={showAlert}
                onDidDismiss={() => setShowAlert(false)}
                header={props.friendlyName}
                inputs={[
                    {
                        name: props.field,
                        type: 'text',
                        id: `content-${props.field}`,
                        value: props.produit[props.field],
                        placeholder: 'Your ' + props.friendlyName
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (alertData) => update(alertData[props.field])
                    }
                ]} />
        </IonItem>
    )
}

export default ContentInputItems