import {
    IonAlert,
    IonBackButton,
    IonButton,
    IonButtons,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonListHeader,
    IonNote,
    IonPage,
    IonRow,
    IonSelect,
    IonSelectOption,
    IonSlide,
    IonSlides,
    IonTitle,
    IonToolbar
  } from '@ionic/react';
  import { camera, trashOutline } from 'ionicons/icons';
  import React, { useContext, useEffect, useState } from 'react';
  
  import { useHistory, useParams } from 'react-router-dom';
  import ContentInputItems from '../components/ProduitInputItems';
  import ContentInputItemsNotes from '../components/ProduitInputNotes';
  import AppContext from '../data/app-context';  
  import { ROUTE_HOME } from '../nav/Routes';
  import { Plugins, CameraResultType, CameraSource, Filesystem, FilesystemDirectory } from '@capacitor/core';
  import ResponsiveContent from '../components/ResponsiveContent';
  import { base64FromPath } from '@ionic/react-hooks/filesystem';
  import { type } from 'os';
  
  const { Camera } = Plugins;
  
  const Fiche: React.FC = () => {
    const history = useHistory();
  const id = useParams<{ id: string }>().id;
  const [showAlert, setShowAlert] = useState(false);
  const [picturesBase64, setPicturesBase64] = useState<string[]>();
  const appCtx = useContext(AppContext)

  const produit = appCtx.produit.find(produit => produit.id === id)


  const deleteHandler = () => {
    console.log(produit)
    if (produit?.id) {
      appCtx.deleteProduit(produit.id)
      history.goBack();
    }
  }

  const updateName = (newName: string) => {
    if (!produit || !newName) return
    let updatedProduit = { ...produit }
    updatedProduit.name = newName;
    appCtx.updateProduit(updatedProduit);
  }

  const updateBase64 = async () => {
    if (produit?.pictures !== undefined && produit?.pictures?.length > 0) {
      const listBase64 = await Promise.all(produit.pictures.map(async (filename) => {
        const file = await Filesystem.readFile({
          path: filename,
          directory: FilesystemDirectory.Data
        })
        return 'data:image/jpeg;base64,' + file.data
      }))
      setPicturesBase64(listBase64)
    } else {
      setPicturesBase64([])
    }
  }

  const takePhotoHandler = async () => {
    if (!produit) return
    const photo = await Camera.getPhoto({
      quality: 80,
      resultType: CameraResultType.Uri,
      source: CameraSource.Prompt,
      width: 500,
    });

    if (!photo || !photo.webPath) return

    const base64 = await base64FromPath(photo.webPath)
    const fileName = new Date().getTime() + '.jpeg'
    await Filesystem.writeFile({
      path: fileName,
      data: base64,
      directory: FilesystemDirectory.Data
    })

    let updatedProduit = { ...produit }
    updatedProduit.pictures = [fileName];
    appCtx.updateProduit(updatedProduit)
  }

  useEffect(() => {
    updateBase64()
  }, [produit?.pictures])


    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonButtons slot='start'>
              <IonBackButton defaultHref={ROUTE_HOME} />
            </IonButtons>
            
          </IonToolbar>
        </IonHeader>
  
   
        <IonContent className="ion-padding-bottom" >
       
             
  
            <IonGrid>
              <IonRow>
                <ResponsiveContent>
                  <IonGrid>
                    <IonRow className="ion-align-items-center">
                      <IonCol style={{ color: "grey" }}>
                        
                  </IonCol>
                      <IonCol className="ion-text-end">
                        <IonButton size="small" fill="outline">
                          <IonIcon icon={camera} />
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                  <IonList className="ion-padding-vertical" mode="ios">                    
                    <IonItem>
                      <IonLabel >
                        
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                    <IonLabel >
                      
                      </IonLabel>
                        
                    </IonItem>
                    <IonItem>
                      <IonLabel >
                       
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                      <IonLabel>
                        
                      </IonLabel>
                    </IonItem>
                    <IonItem>
                      <IonLabel >
                        
                      </IonLabel>
                    </IonItem>
                  </IonList>
                  <IonGrid className="ion-margin-top">
                    <IonRow>
                      <IonCol className="ion-text-center">
                        <IonButton fill="outline" size="small" color="danger">
                          <IonIcon icon={trashOutline} slot="icon-only" />
                        </IonButton>
                      </IonCol>
                    </IonRow>
                  </IonGrid>
                </ResponsiveContent>
              </IonRow>
            </IonGrid>
          </IonContent>
        
  
        
      </IonPage>
    );
  };
  
  export default Fiche;
  