import {
  IonCol,
  IonContent,
  IonFab,
  IonFabButton,
  IonGrid,
  IonHeader,
  IonIcon,
  
  IonImg,
  
  IonItem,
  
  IonLabel,
  
  IonList,
  
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonThumbnail,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import React, { useContext, useState } from 'react';

import { add } from 'ionicons/icons';
import ProduitCard from '../components/ProduitCard';
import ContentModal from '../components/AddContentModal';
import AppContext from '../data/app-context';
import ResponsiveContent from '../components/ResponsiveContent';




const Home:React.FC<{ produitId: string }> = (props) => {
  const [showModal, setShowModal] = useState(false);
  const appCtx = useContext(AppContext);  
 
  return (
    <IonPage>
      <ContentModal showModal={showModal} setShowModal={setShowModal} />
      <IonHeader>
        <IonToolbar>
          <IonTitle>Home</IonTitle>
        </IonToolbar>
      </IonHeader>
      
      
      <IonContent className="ion-padding">
        <IonGrid className="ion-no-padding">
          <IonRow>
            <ResponsiveContent>
              <IonGrid>
                <IonRow>
                  {
                    appCtx.produit.length > 0 ?
                      appCtx.produit.map((produit, index) => (
                        <IonCol size="12" sizeSm="6" sizeXl="4" key={index}>
                          <ProduitCard produitId={produit.id} />
                        </IonCol>
                      ))
                      :
                      <h3 className="ion-text-center">
                        Nothing to show yet, add your first content using the button below:
                      </h3>
                  }
                </IonRow>
              </IonGrid>
            </ResponsiveContent>
          </IonRow>
        </IonGrid>
        <IonFab vertical={appCtx.produit.length > 0 ? "bottom" : "center"} horizontal={appCtx.produit.length > 0 ? "end" : "center"} slot="fixed">
          <IonFabButton onClick={() => setShowModal(true)}>
            <IonIcon icon={add} />
          </IonFabButton>
        </IonFab>
      </IonContent>
    </IonPage>
  );
};

export default Home;
