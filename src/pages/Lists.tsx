import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonFab,
    IonFabButton,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonPage,
    IonRow,
    IonTitle,
    IonToolbar
  } from '@ionic/react';
  import React, { useContext, useState } from 'react';
  
  import { add } from 'ionicons/icons';
  import AppContext from '../data/app-context';
import ResponsiveContent from '../components/ResponsiveContent';
  
  
  const Lists: React.FC = () => {
    const [showModal, setShowModal] = useState(false);
    const appCtx = useContext(AppContext);
    return (
      <IonPage>
        <IonHeader>
          <IonToolbar>
            <IonTitle>Home</IonTitle>
          </IonToolbar>
        </IonHeader>
        
        
        <IonContent className="ion-padding">
          
        </IonContent>
      </IonPage>
    );
  };
  
  export default Lists;
  