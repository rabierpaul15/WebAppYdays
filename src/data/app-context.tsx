import React from 'react';

export interface Produit {
    id: string,
    name: string,
    price: number,
    stock: number,    
    pictures: string[] | undefined,
    DLC: string;


}

export interface Profile {
    id: string,
    username: string,
    picture: string | null,
    
}


export type ProduitInputFields =  "name" | "price" | "stock"  | "DLC"; 
export const defaultProfile: Profile = {
    id: '0',
    username: "Unknown",
    picture: null,
    
}

interface AppContext {
    initContext: () => void,
    produit: Produit[],
    addProduit: (newProduit: Produit) => void,
    deleteProduit: (id: string) => void,
    updateProduit: (updatedProduit: Produit) => void,
    profile: Profile,
    updateProfile: (updatedProfile: Profile) => void
}

const AppContext = React.createContext<AppContext>({
    initContext: () => { },
    produit: [],
    addProduit: () => { },
    deleteProduit: () => { },
    updateProduit: () => { },
    profile: defaultProfile,
    updateProfile: () => { }
});

export default AppContext