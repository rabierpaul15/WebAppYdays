import React, { useState, useEffect, useRef } from 'react';
import AppContext, { Produit, Profile, defaultProfile } from './app-context';

import { Plugins } from '@capacitor/core'

const { Storage, Filesystem } = Plugins;

const AppContextProvider: React.FC = (props) => {
    const [produit, setproduit] = useState<Produit[]>([])
    const [profile, setProfile] = useState<Profile>(defaultProfile)
    const didMountRef = useRef(false);

    useEffect(() => {
        if (didMountRef.current) {
            console.log(profile)
            Storage.set({ key: 'profile', value: JSON.stringify(profile) })
            Storage.set({ key: 'produit', value: JSON.stringify(produit) })
        } else {
            didMountRef.current = true;
        }
    }, [profile, produit])

    const addProduit = (newProduit: Produit) => {
        setproduit((prevState) => {
            let newList = [...prevState];
            newList.unshift(newProduit)
            return newList
        })
    }

    const deleteProduit = (id: string) => {
        const index = produit.map(el => el.id).indexOf(id)
        setproduit((prevState) => {
            let newList = [...prevState];
            newList.splice(index, 1)
            return newList
        })
    }

    const updateProduit = (updateProduit: Produit) => {
        const index = produit.map(el => el.id).indexOf(updateProduit.id)
        setproduit((prevState) => {
            let newList = [...prevState];
            newList.splice(index, 1, updateProduit)
            return newList
        })
    }

    const updateProfile = (updateProfile: Profile) => {
        setProfile(updateProfile)
    }

    const initContext = async () => {
        const profileData = await Storage.get({ key: 'profile' })
        const produitsData = await Storage.get({ key: 'produit' })
        const storedProfile = profileData.value ? JSON.parse(profileData.value) : defaultProfile;
        const storedProduit = produitsData.value ? JSON.parse(produitsData.value) : [];
        didMountRef.current = false;
        setProfile(storedProfile)
        setproduit(storedProduit)
    }

    return <AppContext.Provider value={{ initContext, produit, profile, updateProfile, addProduit, deleteProduit, updateProduit }}>
        {props.children}
    </AppContext.Provider>
}

export default AppContextProvider