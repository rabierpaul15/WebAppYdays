export const ROUTE_PAGES_BASE = '/pages/';
export const ROUTE_LISTES = ROUTE_PAGES_BASE + 'Lists/'; 
export const ROUTE_PROFILE =  ROUTE_PAGES_BASE + 'Profile/';
export const ROUTE_HOME =  ROUTE_PAGES_BASE + 'Home/';
export const ROUTE_FICHE =  ROUTE_PAGES_BASE + 'Fiche/';
