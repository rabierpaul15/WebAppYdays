import { IonContent, IonPage, IonSpinner } from "@ionic/react";
import React, { useContext } from "react";
import { RouteComponentProps } from "react-router";
import { Route, Redirect } from "react-router-dom";
import AppContext from "../data/app-context";

export interface RouteAndRedirectProps {
    component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
    path?: string;
    exact?: boolean;
}



